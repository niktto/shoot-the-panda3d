from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from math import pi, sin, cos

class DemoPandy(ShowBase):

    def __init__(self):
        ShowBase.__init__(self) #  super nie dziala ze wzgledu na ShowBase
        
        # Laduje pomniejszony model "srodowiska" bedacy w podstawowej palecie
        self.background = self.load_static_model(
            'models/environment',
            (0.25, 0.25, 0.25),
            (-8,42,0)
        )

        # Laduje nasz model kurczaka
        self.kurczak = self.load_static_model('kurczak.x')
	#kurczak = self.kurczak
	#print(type(kurczak))
	#print(dir(kurczak))

        # Obraca kamere za pomoca metody spinCameraTask
        self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")

        self.accept('arrow_up-repeat', self.forward)
        self.accept('arrow_down-repeat', self.back)
        self.accept('arrow_left-repeat', self.left)
        self.accept('arrow_right-repeat', self.right)

    def forward(self, *button):
        self.kurczak.setY(self.kurczak, 0.2)

    def back(self, *button):
        self.kurczak.setY(self.kurczak, -0.2)

    def left(self, *button):
        self.kurczak.setX(self.kurczak, -0.2)
        
    def right(self, *button):
        self.kurczak.setX(self.kurczak, 0.2)

    def load_static_model(self, model_path, scale_tuple=(1,1,1),
            position_tuple=(0,0,0)):
        '''Laduje model ;)
        Attributes:
        model_path - string
        scale_tuple - (x,y,z)
        position_tuple - (x,y,z)
        '''
        try:
            model = self.loader.loadModel(model_path)
        except IOError:
            return False
        model.setScale(scale_tuple)
        model.setPos(position_tuple)
        model.reparentTo(self.render)
        return model

    def spinCameraTask(self, task):
        """Kamera kreci sie jak w domyslnym demie (nie kazcie mi tego
        przerabiac, to matematyka :()) patrzac caly czas na kurczaka """
#        angleDegrees = 90# * 6.0
#        angleRadians = angleDegrees * (pi / 180.0)
	kur_pos = self.kurczak.getPos()
        self.camera.setPos(kur_pos[0] , kur_pos[1]  , 30)# * cos(angleRadians)
        self.camera.lookAt(self.kurczak)
        return Task.cont


apka = DemoPandy()
apka.run()
